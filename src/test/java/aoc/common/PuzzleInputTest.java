package aoc.common;

import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

class PuzzleInputTest {

    @Test
    void dimensions() {
        PuzzleInput input = PuzzleInput.fromString("""
                0123
                0123
                0123
                """);
        assertThat(input.getHeight()).isEqualTo(3);
        assertThat(input.getWidth()).isEqualTo(4);
    }

}