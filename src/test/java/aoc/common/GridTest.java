package aoc.common;

import org.junit.jupiter.api.Test;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

class GridTest {

    @Test
    void should_parse_grid() {
        Grid<Integer> actual = Grid.parse(List.of("123", "456"), Integer::parseInt);
        Grid<Integer> expected = new Grid<>(List.of(
                List.of(1, 2, 3),
                List.of(4, 5, 6)
        ), 2, 3);
        assertThat(actual).isEqualTo(expected);
    }

}