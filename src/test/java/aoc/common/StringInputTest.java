package aoc.common;

import org.junit.jupiter.api.Test;

import java.util.List;

import static aoc.common.StringInput.input;
import static org.assertj.core.api.Assertions.assertThat;

class StringInputTest {

    @Test
    void substring_within_bounds() {
        StringInput input = input("012345");
        assertThat(input.substringWithinBounds(1, 4)).isEqualTo("123");
        assertThat(input.substringWithinBounds(-1, 4)).isEqualTo("0123");
        assertThat(input.substringWithinBounds(2, 10)).isEqualTo("2345");
    }

    @Test
    void match_regex() {
        List<RegexMatch> matches = input("1 blue, 3 red, 2 green").matchRegex("(\\d+) ([a-z]+),?");

        assertThat(matches).containsExactly(
                RegexMatch.of("1", "blue"),
                RegexMatch.of("3", "red"),
                RegexMatch.of("2", "green")
        );
    }

}