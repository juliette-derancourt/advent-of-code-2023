package aoc.common;

import org.junit.jupiter.api.Test;

import java.util.Set;

import static org.assertj.core.api.Assertions.assertThat;

class CoordinatesTest {

    @Test
    void get_neighbours() {
        Set<Coordinates> neighbours = new Coordinates(1, 2).getNeighboursWithinBounds(5, 5);
        assertThat(neighbours).hasSize(8);
    }

    @Test
    void get_neighbours_corner() {
        Set<Coordinates> neighbours = new Coordinates(0, 0).getNeighboursWithinBounds(2, 2);
        assertThat(neighbours).hasSize(3)
                .contains(
                        new Coordinates(0, 1),
                        new Coordinates(1, 0),
                        new Coordinates(1, 1)
                );
    }

}