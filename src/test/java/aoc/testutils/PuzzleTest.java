package aoc.testutils;

import aoc.PuzzleResolver;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
public @interface PuzzleTest {

    int number();

    String name();

    Class<? extends PuzzleResolver<?>> resolver();

}
