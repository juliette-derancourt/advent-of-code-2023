package aoc.testutils.fetching;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URI;
import java.net.URL;
import java.util.Properties;

public class AocClient {

    private static final String AOC_URL = "https://adventofcode.com/2023/";

    public String fetchInput(int puzzleNumber) throws Exception {
        URI uri = new URI(AOC_URL)
                .resolve("day/")
                .resolve(puzzleNumber + "/")
                .resolve("input");
        return fetch(uri);
    }

    private String fetch(URI uri) throws IOException {
        HttpURLConnection connection = openConnection(uri);

        if (connection.getResponseCode() != 200) {
            System.err.println("Error while fetching data: " + connection.getResponseCode());
        }

        String response = readResponse(connection.getInputStream());
        connection.disconnect();

        return response;
    }

    private HttpURLConnection openConnection(URI uri) throws IOException {
        URL url = uri.toURL();
        HttpURLConnection connection = (HttpURLConnection) url.openConnection();
        connection.setRequestMethod("GET");
        connection.setRequestProperty("Cookie", "session=" + getSessionCookie());
        System.out.println("GET " + url);
        return connection;
    }

    private static String getSessionCookie() {
        String propertiesFileName = "aoc.properties";
        try {
            Properties properties = new Properties();
            properties.load(PuzzleDataFolder.class.getClassLoader().getResourceAsStream(propertiesFileName));
            return properties.getProperty("session");
        } catch (IOException e) {
            throw new RuntimeException("The session cookie must be configured in " + propertiesFileName);
        }
    }

    private String readResponse(InputStream inputStream) throws IOException {
        BufferedReader in = new BufferedReader(new InputStreamReader(inputStream));
        StringBuilder response = new StringBuilder();

        String line = in.readLine();
        response.append(line);
        while ((line = in.readLine()) != null) {
            response.append("\n");
            response.append(line);
        }

        in.close();
        return response.toString();
    }

}
