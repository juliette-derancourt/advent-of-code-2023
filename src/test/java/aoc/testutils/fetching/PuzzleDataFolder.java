package aoc.testutils.fetching;

import aoc.testutils.PuzzlePart;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;

public class PuzzleDataFolder {

    private final AocClient aocClient;
    private final int puzzleNumber;
    private final File folder;

    public PuzzleDataFolder(int puzzleNumber) {
        this.aocClient = new AocClient();
        this.puzzleNumber = puzzleNumber;
        this.folder = new File("src/test/resources/data/puzzle" + puzzleNumber);
    }

    public static void initFor(int puzzleNumber) {
        new PuzzleDataFolder(puzzleNumber).init();
    }

    public void init() {
        if (!folder.exists()) {
            folder.mkdir();
        }
        createInputFile();
        createFilesForParts();
    }

    private void createInputFile() {
        String fileName = "input.txt";
        if (hasFile(fileName)) {
            return;
        }
        try {
            String input = aocClient.fetchInput(puzzleNumber);
            writeInFile(input, fileName);
        } catch (Exception e) {
            System.err.println("Unable to create data files for puzzle " + puzzleNumber);
        }
    }

    private void createFilesForParts() {
        try {
            for (PuzzlePart part : PuzzlePart.values()) {
                File partFolder = new File(folder, "part" + part);
                writeInFile(partFolder, """
                        
                        ----------
                        """, "example.txt");
                writeInFile(partFolder, "", "correct-answers.txt");
                writeInFile(partFolder, "", "incorrect-answers.txt");
            }
        } catch (IOException e) {
            System.err.println("Unable to create example files for puzzle " + puzzleNumber);
        }
    }

    private boolean hasFile(String fileName) {
        return new File(folder, fileName).exists();
    }

    private void writeInFile(String input, String fileName) throws IOException {
        writeInFile(folder, input, fileName);
    }

    private void writeInFile(File folder, String input, String fileName) throws IOException {
        File newFile = new File(folder, fileName);
        if (newFile.exists()) {
            return;
        }
        if (!folder.exists()) {
            folder.mkdir();
        }
        Files.writeString(newFile.toPath(), input);
    }

}
