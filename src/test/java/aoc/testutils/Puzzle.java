package aoc.testutils;

import aoc.PuzzleResolver;
import aoc.common.PuzzleInput;
import aoc.testutils.fetching.PuzzleDataFolder;
import aoc.testutils.parsing.Answers;
import aoc.testutils.parsing.Example;
import org.junit.platform.commons.support.ReflectionSupport;

import java.util.List;

import static aoc.testutils.parsing.ResourceUtils.readPuzzleData;

public record Puzzle(int number, PuzzleResolver<?> resolver) {

    public static Puzzle buildFrom(PuzzleTest annotation) {
        PuzzleResolver<?> resolver = ReflectionSupport.newInstance(annotation.resolver());
        return new Puzzle(annotation.number(), resolver);
    }

    public void initData() {
        PuzzleDataFolder.initFor(number);
    }

    public Example getExampleFor(PuzzlePart part) {
        List<String> fileContent = readPuzzleData(number, part, "example.txt");
        return Example.of(fileContent);
    }

    public PuzzleInput getInput() {
        List<String> lines = readPuzzleData(number, "input.txt");
        return PuzzleInput.withLines(lines);
    }

    public Answers getPreviousAnswers(PuzzlePart part) {
        List<String> correct = readPuzzleData(number, part, "correct-answers.txt");
        List<String> incorrect = readPuzzleData(number, part, "incorrect-answers.txt");
        return new Answers(correct, incorrect);
    }

}
