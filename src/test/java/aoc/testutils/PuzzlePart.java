package aoc.testutils;

public enum PuzzlePart {

    ONE(1),
    TWO(2);

    private final int intValue;

    PuzzlePart(int intValue) {
        this.intValue = intValue;
    }

    public int toInt() {
        return intValue;
    }

    @Override
    public String toString() {
        return String.valueOf(intValue);
    }

}
