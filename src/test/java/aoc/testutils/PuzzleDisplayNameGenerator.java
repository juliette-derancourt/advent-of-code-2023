package aoc.testutils;

import org.junit.jupiter.api.DisplayNameGenerator;
import org.junit.platform.commons.support.AnnotationSupport;

import java.lang.reflect.Method;

public class PuzzleDisplayNameGenerator implements DisplayNameGenerator {

    @Override
    public String generateDisplayNameForClass(Class<?> testClass) {
        return AnnotationSupport.findAnnotation(testClass, PuzzleTest.class)
                .map(puzzle -> String.format("Puzzle #%d: %s", puzzle.number(), puzzle.name()))
                .orElse(null);
    }

    @Override
    public String generateDisplayNameForNestedClass(Class<?> nestedClass) {
        return null;
    }

    @Override
    public String generateDisplayNameForMethod(Class<?> testClass, Method testMethod) {
        return null;
    }

}
