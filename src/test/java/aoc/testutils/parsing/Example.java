package aoc.testutils.parsing;

import aoc.common.PuzzleInput;

import java.util.List;

public record Example(String solution, PuzzleInput input) {

    public static Example of(List<String> exampleFileContent) {
        if (exampleFileContent.size() < 3) {
            throw new RuntimeException("Example data are not available");
        }
        String solution = exampleFileContent.get(0);
        List<String> inputLines = exampleFileContent.subList(2, exampleFileContent.size());
        return new Example(solution, PuzzleInput.withLines(inputLines));
    }

}
