package aoc.testutils.parsing;

import aoc.testutils.PuzzlePart;

import java.net.URI;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

public class ResourceUtils {

    public static List<String> readPuzzleData(int puzzleNumber, PuzzlePart part, String fileName) {
        String path = String.format("data/puzzle%d/part%d/%s", puzzleNumber, part.toInt(), fileName);
        return readFileLines(path);
    }

    public static List<String> readPuzzleData(int puzzleNumber, String fileName) {
        String path = String.format("data/puzzle%d/%s", puzzleNumber, fileName);
        return readFileLines(path);
    }

    private static List<String> readFileLines(String filePath) {
        try {
            URI uri = ResourceUtils.class.getClassLoader().getResource(filePath).toURI();
            return Files.readAllLines(Paths.get(uri));
        } catch (Exception e) {
            return List.of();
        }
    }

}
