package aoc.testutils.parsing;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

public record Answers(List<String> correct, List<String> incorrect) {

    public void verifySolution(Object solution) {
        String solutionAsString = String.valueOf(solution);
        incorrect.forEach(answer -> assertThat(solutionAsString).isNotEqualTo(answer));
        correct.forEach(answer -> assertThat(solutionAsString).isEqualTo(answer));
    }

}
