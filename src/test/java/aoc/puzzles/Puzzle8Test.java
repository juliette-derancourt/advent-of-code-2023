package aoc.puzzles;

import aoc.PuzzleResolverTest;
import aoc.testutils.PuzzleTest;

@PuzzleTest(
        number = 8,
        name = "Haunted Wasteland",
        resolver = Puzzle8.class
)
public class Puzzle8Test extends PuzzleResolverTest {

}