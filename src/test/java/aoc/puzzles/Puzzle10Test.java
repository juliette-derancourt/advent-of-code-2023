package aoc.puzzles;

import aoc.PuzzleResolverTest;
import aoc.testutils.PuzzleTest;

@PuzzleTest(
        number = 10,
        name = "Pipe Maze",
        resolver = Puzzle10.class
)
public class Puzzle10Test extends PuzzleResolverTest {

}