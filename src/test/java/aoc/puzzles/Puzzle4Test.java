package aoc.puzzles;

import aoc.PuzzleResolverTest;
import aoc.common.StringInput;
import aoc.testutils.PuzzleTest;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

@PuzzleTest(
        number = 4,
        name = "Scratchcards",
        resolver = Puzzle4.class
)
public class Puzzle4Test extends PuzzleResolverTest {

    @Test
    void getWinCopiesNumbers() {
        Puzzle4.Card card = Puzzle4.Card.parse(StringInput.input("Card 1: 41 48 83 86 17 | 83 86  6 31 17  9 48 53"));
        assertThat(card.getWinCopiesNumbers()).containsExactly(2, 3, 4, 5);
    }

}
