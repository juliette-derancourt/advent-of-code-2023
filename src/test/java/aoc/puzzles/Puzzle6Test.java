package aoc.puzzles;

import aoc.PuzzleResolverTest;
import aoc.testutils.PuzzleTest;

@PuzzleTest(
        number = 6,
        name = "Wait For It",
        resolver = Puzzle6.class
)
public class Puzzle6Test extends PuzzleResolverTest {

}