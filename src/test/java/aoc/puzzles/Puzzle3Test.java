package aoc.puzzles;

import aoc.PuzzleResolverTest;
import aoc.common.Coordinates;
import aoc.common.PuzzleInput;
import aoc.puzzles.Puzzle3.EngineElement;
import aoc.testutils.PuzzleTest;
import org.junit.jupiter.api.Test;

import java.util.List;
import java.util.Set;

import static aoc.common.PuzzleInput.fromString;
import static aoc.puzzles.Puzzle3.ElementType.NUMBER;
import static aoc.puzzles.Puzzle3.ElementType.SYMBOL;
import static aoc.puzzles.Puzzle3.Engine;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertTrue;

@PuzzleTest(
        number = 3,
        name = "Gear Ratios",
        resolver = Puzzle3.class
)
class Puzzle3Test extends PuzzleResolverTest {

    private final PuzzleInput input = fromString("""
                12..
                $..3
                """);

    @Test
    void parse_input() {
        Engine engine = Engine.parse(input);
        assertThat(engine.elements())
                .hasSize(3)
                .contains(
                        new EngineElement(NUMBER, "12", new Coordinates(0, 0)),
                        new EngineElement(SYMBOL, "$", new Coordinates(1, 0)),
                        new EngineElement(NUMBER, "3", new Coordinates(1, 3))
                );
    }

    @Test
    void neighbours() {
        EngineElement element = new EngineElement(NUMBER, "12", new Coordinates(0, 0));
        Set<Coordinates> neighbours = element.getAllNeighboursIn(input);
        assertThat(neighbours).hasSize(4)
                .contains(
                        new Coordinates(0, 2),
                        new Coordinates(1, 0),
                        new Coordinates(1, 1),
                        new Coordinates(1, 2)
                );
    }

    @Test
    void symbol_exists_at() {
        Engine engine = Engine.parse(input);
        assertTrue(engine.symbolExistsAt(new Coordinates(1, 0)));
    }

    @Test
    void get_part_numbers() {
        Engine engine = Engine.parse(input);
        List<EngineElement> partNumbers = engine.getPartNumbers();
        assertThat(partNumbers).hasSize(1);
    }

}