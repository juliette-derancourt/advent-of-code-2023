package aoc.puzzles;

import aoc.PuzzleResolverTest;
import aoc.common.StringInput;
import aoc.testutils.PuzzleTest;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.List;

import static aoc.puzzles.Puzzle7.Hand;
import static org.assertj.core.api.Assertions.assertThat;

@PuzzleTest(
        number = 7,
        name = "Camel Cards",
        resolver = Puzzle7.class
)
public class Puzzle7Test extends PuzzleResolverTest {

    @Test
    void compare_hands() {
        List<Hand> hands = hands(
                "QT432", "AQTK2", "AKQT2",
                "AKQTJ", "AAKQT",
                "AAKK5",
                "AAQJT", "AAAQT",
                "AAAKK",
                "QAAAA", "AAAQJ", "AAAAT",
                "AAAAJ", "AAAAA");
        List<Hand> sortedHands = hands.stream().sorted().toList();
        assertThat(sortedHands).isEqualTo(hands);
    }

    private List<Hand> hands(String... hands) {
        return Arrays.stream(hands).map(StringInput::input).map(h -> Hand.parse(h, Puzzle7.ParsingType.CONSIDER_JOKERS)).toList();
    }

}