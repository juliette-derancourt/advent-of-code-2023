package aoc.puzzles;

import aoc.PuzzleResolverTest;
import aoc.common.StringInput;
import aoc.testutils.PuzzleTest;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

@PuzzleTest(
        number = 1,
        name = "Trebuchet?!",
        resolver = Puzzle1.class
)
public class Puzzle1Test extends PuzzleResolverTest {

    @Test
    void findFirstDigit() {
        assertThat(calibration("123").findFirstDigit()).contains("1");
        assertThat(calibration("one2three").findFirstDigit()).contains("1");
        assertThat(calibration("zeudhzoed").findFirstDigit()).isEmpty();
        assertThat(calibration("aidheseven").findFirstDigit()).contains("7");
        assertThat(calibration("eightwo").findFirstDigit()).contains("8");
    }

    @Test
    void findLastDigit() {
        assertThat(calibration("123").findLastDigit()).contains("3");
        assertThat(calibration("one2three").findLastDigit()).contains("3");
        assertThat(calibration("zeudhzoed").findLastDigit()).isEmpty();
        assertThat(calibration("aidheseven").findLastDigit()).contains("7");
        assertThat(calibration("eightwo").findLastDigit()).contains("2");
    }

    private static Puzzle1.Calibration calibration(String line) {
        return new Puzzle1.Calibration(new StringInput(line), true);
    }

}
