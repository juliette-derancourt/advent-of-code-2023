package aoc.puzzles;

import aoc.PuzzleResolverTest;
import aoc.testutils.PuzzleTest;

@PuzzleTest(
        number = 11,
        name = "Cosmic Expansion",
        resolver = Puzzle11.class
)
public class Puzzle11Test extends PuzzleResolverTest {

}