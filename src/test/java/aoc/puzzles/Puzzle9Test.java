package aoc.puzzles;

import aoc.PuzzleResolverTest;
import aoc.testutils.PuzzleTest;

@PuzzleTest(
        number = 9,
        name = "Mirage Maintenance",
        resolver = Puzzle9.class
)
public class Puzzle9Test extends PuzzleResolverTest {

}