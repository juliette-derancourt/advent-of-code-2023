package aoc.puzzles;

import aoc.PuzzleResolverTest;
import aoc.testutils.PuzzleTest;

@PuzzleTest(
        number = 15,
        name = "Lens Library",
        resolver = Puzzle15.class
)
public class Puzzle15Test extends PuzzleResolverTest {

}