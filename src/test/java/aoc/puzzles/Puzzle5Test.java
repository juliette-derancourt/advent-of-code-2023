package aoc.puzzles;

import aoc.PuzzleResolverTest;
import aoc.testutils.PuzzleTest;

@PuzzleTest(
        number = 5,
        name = "If You Give A Seed A Fertilizer",
        resolver = Puzzle5.class
)
public class Puzzle5Test extends PuzzleResolverTest {

}
