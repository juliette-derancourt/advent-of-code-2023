package aoc.puzzles;

import aoc.PuzzleResolverTest;
import aoc.testutils.PuzzleTest;

@PuzzleTest(
        number = 13,
        name = "Point of Incidence",
        resolver = Puzzle13.class
)
public class Puzzle13Test extends PuzzleResolverTest {

}