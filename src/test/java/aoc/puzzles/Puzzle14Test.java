package aoc.puzzles;

import aoc.PuzzleResolverTest;
import aoc.testutils.PuzzlePart;
import aoc.testutils.PuzzleTest;
import org.junit.jupiter.api.Test;

import static aoc.puzzles.Puzzle14.Platform;
import static org.assertj.core.api.Assertions.assertThat;

@PuzzleTest(
        number = 14,
        name = "Parabolic Reflector Dish",
        resolver = Puzzle14.class
)
public class Puzzle14Test extends PuzzleResolverTest {

    @Test
    void spin_cycles() {
        Platform platform = new Platform(puzzle.getExampleFor(PuzzlePart.TWO).input().toGrid());

        assertCycleResult(platform, "first cycle", """
                .....#....
                ....#...O#
                ...OO##...
                .OO#......
                .....OOO#.
                .O#...O#.#
                ....O#....
                ......OOOO
                #...O###..
                #..OO#....
                """);

        assertCycleResult(platform, "second cycle", """
                .....#....
                ....#...O#
                .....##...
                ..O#......
                .....OOO#.
                .O#...O#.#
                ....O#...O
                .......OOO
                #..OO###..
                #.OOO#...O
                """);

        assertCycleResult(platform, "third cycle", """
                .....#....
                ....#...O#
                .....##...
                ..O#......
                .....OOO#.
                .O#...O#.#
                ....O#...O
                .......OOO
                #...O###.O
                #.OOO#...O
                """);
    }

    private static void assertCycleResult(Platform platform, String step, String expected) {
        platform.spinCycle();
        assertThat(platform.grid().print())
                .as(step)
                .isEqualToIgnoringNewLines(expected);
    }

}