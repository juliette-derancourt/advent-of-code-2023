package aoc.puzzles;

import aoc.PuzzleResolverTest;
import aoc.testutils.PuzzleTest;

@PuzzleTest(
        number = 12,
        name = "Hot Springs",
        resolver = Puzzle12.class
)
public class Puzzle12Test extends PuzzleResolverTest {

}