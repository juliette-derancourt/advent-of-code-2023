package aoc.puzzles;

import aoc.PuzzleResolverTest;
import aoc.common.StringInput;
import aoc.testutils.PuzzleTest;
import org.junit.jupiter.api.Test;

import java.util.List;

import static aoc.common.StringInput.input;
import static aoc.puzzles.Puzzle2.BagContent;
import static aoc.puzzles.Puzzle2.Color;
import static aoc.puzzles.Puzzle2.CubeSet;
import static aoc.puzzles.Puzzle2.Game;
import static aoc.puzzles.Puzzle2.GameSet;
import static org.assertj.core.api.Assertions.assertThat;

@PuzzleTest(
        number = 2,
        name = "Cube Conundrum",
        resolver = Puzzle2.class
)
public class Puzzle2Test extends PuzzleResolverTest {

    @Test
    void parse_game() {
        Game expectedGame = new Game(11, List.of(
                new GameSet(List.of(new CubeSet(3, Color.BLUE), new CubeSet(4, Color.RED))),
                new GameSet(List.of(new CubeSet(1, Color.RED), new CubeSet(2, Color.GREEN), new CubeSet(6, Color.BLUE))),
                new GameSet(List.of(new CubeSet(2, Color.GREEN)))
        ));
        StringInput input = input("Game 11: 3 blue, 4 red; 1 red, 2 green, 6 blue; 2 green");
        assertThat(Game.parse(input)).isEqualTo(expectedGame);
    }

    @Test
    void get_minimal_bag_content() {
        StringInput input = input("Game 1: 3 blue, 4 red; 1 red, 2 green, 6 blue; 2 green");
        BagContent expected = new BagContent(List.of(
                new CubeSet(4, Color.RED),
                new CubeSet(6, Color.BLUE),
                new CubeSet(2, Color.GREEN)
        ));
        assertThat(Game.parse(input).getMinimalBagContent()).isEqualTo(expected);
    }

}
