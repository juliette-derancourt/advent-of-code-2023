package aoc;

import aoc.common.PuzzleInput;
import aoc.testutils.Puzzle;
import aoc.testutils.PuzzleDisplayNameGenerator;
import aoc.testutils.PuzzlePart;
import aoc.testutils.PuzzleTest;
import aoc.testutils.parsing.Example;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.DisplayNameGeneration;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.TestMethodOrder;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.EnumSource;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assumptions.assumeFalse;
import static org.junit.platform.commons.support.AnnotationSupport.findAnnotation;

@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
@DisplayNameGeneration(PuzzleDisplayNameGenerator.class)
public abstract class PuzzleResolverTest {

    protected final Puzzle puzzle;

    public PuzzleResolverTest() {
        PuzzleTest puzzleTest = findAnnotation(getClass(), PuzzleTest.class).orElseThrow();
        this.puzzle = Puzzle.buildFrom(puzzleTest);
        puzzle.initData();
    }

    @ParameterizedTest(name = "example for part {0}")
    @EnumSource(PuzzlePart.class)
    @DisplayName("Examples")
    @Order(Integer.MAX_VALUE - 1)
    void examples(PuzzlePart part) {
        Example example = puzzle.getExampleFor(part);
        String result = getSolutionForPart(part, example.input());
        assertThat(String.valueOf(result)).isEqualTo(example.solution());
    }

    @ParameterizedTest(name = "solution for part {0}")
    @EnumSource(PuzzlePart.class)
    @DisplayName("Solutions")
    @Order(Integer.MAX_VALUE)
    void solutions(PuzzlePart part) {
        String result = getSolutionForPart(part, puzzle.getInput());
        puzzle.getPreviousAnswers(part).verifySolution(result);
        System.out.printf("Puzzle %d part %s: %s%n", puzzle.number(), part, result);
    }

    private String getSolutionForPart(PuzzlePart part, PuzzleInput input) {
        Object result = switch (part) {
            case ONE -> puzzle.resolver().part1(input);
            case TWO -> puzzle.resolver().part2(input);
        };
        assumeFalse(result == null, "Part not implemented yet");
        return String.valueOf(result);
    }

}
