package aoc;

import aoc.common.PuzzleInput;

public interface PuzzleResolver<T> {

    T part1(PuzzleInput input);

    T part2(PuzzleInput input);

}
