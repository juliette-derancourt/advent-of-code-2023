package aoc.common;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.function.Function;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public record StringInput(String value) {

    public static StringInput input(String string) {
        return new StringInput(string);
    }

    public int toInt() {
        return Integer.parseInt(value);
    }

    public long toLong() {
        return Long.parseLong(value);
    }

    public StringInput apply(Function<String, String> function) {
        return new StringInput(function.apply(value));
    }

    public String substringWithinBounds(int beginIndex, int endIndex) {
        return value.substring(
                Math.max(0, beginIndex),
                Math.min(endIndex, value.length())
        );
    }

    public Stream<Integer> indexes() {
        return IntStream.rangeClosed(0, value.length()).boxed();
    }

    public List<StringInput> split(String regex) {
        return Arrays.stream(value.split(regex)).map(StringInput::new).toList();
    }

    public List<RegexMatch> matchRegex(String regex) {
        Matcher matcher = getMatcher(regex);
        List<RegexMatch> matchs = new ArrayList<>();
        while (matcher.find()) {
            matchs.add(RegexMatch.from(matcher));
        }
        return matchs;
    }

    public RegexMatch matchRegexOnce(String regex) {
        return matchRegex(regex).get(0);
    }

    public Matcher getMatcher(String regex) {
        Pattern compile = Pattern.compile(regex);
        return compile.matcher(value);
    }

    public List<Integer> extractNumbers() {
        return matchRegex("(-?\\d+)").stream().map(match -> match.getInt(1)).toList();
    }

    public boolean contains(String s) {
        return value.contains(s);
    }

    public String stringAt(int i) {
        return String.valueOf(value.charAt(i));
    }

    public StringInput insert(int i, String s) {
        return new StringInput(new StringBuilder(value).insert(i, s).toString());
    }

}
