package aoc.common;

import java.util.Set;
import java.util.stream.Collectors;

public record Coordinates(int x, int y) {

    public Coordinates moveY(int i) {
        return new Coordinates(x, y + i);
    }

    public Coordinates moveX(int i) {
        return new Coordinates(x + i, y);
    }

    public Coordinates move(Direction direction) {
        return switch (direction) {
            case NORTH -> moveX(-1);
            case WEST -> moveY(-1);
            case SOUTH -> moveX(1);
            case EAST -> moveY(1);
        };
    }

    public Set<Coordinates> getNeighboursWithinBounds(int height, int width) {
        Set<Coordinates> neighbours = Set.of(
                new Coordinates(x + 1, y),
                new Coordinates(x, y + 1),
                new Coordinates(x - 1, y),
                new Coordinates(x, y - 1),
                new Coordinates(x + 1, y + 1),
                new Coordinates(x + 1, y - 1),
                new Coordinates(x - 1, y + 1),
                new Coordinates(x - 1, y - 1)
        );
        return neighbours.stream()
                .filter(n -> n.isWithinBounds(height, width))
                .collect(Collectors.toSet());
    }

    public boolean isWithinBounds(Grid<?> grid) {
        return isWithinBounds(grid.height(), grid.width());
    }

    public boolean isWithinBounds(int height, int width) {
        return x >= 0 && x < height && y >= 0 && y < width;
    }

}
