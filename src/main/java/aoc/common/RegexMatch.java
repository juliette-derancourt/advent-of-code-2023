package aoc.common;

import java.util.Arrays;
import java.util.List;
import java.util.regex.Matcher;

import static java.util.stream.IntStream.rangeClosed;

public record RegexMatch(List<StringInput> groups) {

    static RegexMatch of(String... strings) {
        return new RegexMatch(Arrays.stream(strings).map(StringInput::new).toList());
    }

    static RegexMatch from(Matcher matcher) {
        List<StringInput> groups = rangeClosed(1, matcher.groupCount())
                .mapToObj(matcher::group)
                .map(StringInput::new)
                .toList();
        return new RegexMatch(groups);
    }

    public StringInput get(int group) {
        return groups.get(group - 1);
    }

    public String getString(int group) {
        return get(group).value();
    }

    public int getInt(int group) {
        return get(group).toInt();
    }

}
