package aoc.common;

import java.math.BigInteger;

public class MathUtils {

    public static BigInteger lcm(BigInteger n1, BigInteger n2) {
        BigInteger gcd = n1.gcd(n2);
        return n1.multiply(n2).abs().divide(gcd);
    }

}
