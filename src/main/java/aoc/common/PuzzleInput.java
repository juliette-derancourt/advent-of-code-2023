package aoc.common;

import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public record PuzzleInput(List<StringInput> lines) {

    public static PuzzleInput withLines(List<String> lines) {
        return new PuzzleInput(lines.stream().map(StringInput::new).toList());
    }

    public static PuzzleInput fromString(String string) {
        return withLines(string.lines().toList());
    }

    public Stream<StringInput> streamLines() {
        return lines.stream();
    }

    public StringInput linesAsString() {
        String allLines = lines.stream().map(StringInput::value)
                .collect(Collectors.joining("\n"));
        return StringInput.input(allLines);
    }

    public int getWidth() {
        return lines.get(0).value().length();
    }

    public int getHeight() {
        return lines.size();
    }

    public Grid<String> toGrid() {
        List<String> linesAsString = streamLines().map(StringInput::value).toList();
        return Grid.parse(linesAsString, Function.identity());
    }

    public <T> Grid<T> toGrid(Function<String, T> parser) {
        List<String> linesAsString = streamLines().map(StringInput::value).toList();
        return Grid.parse(linesAsString, parser);
    }

}
