package aoc.common;

public enum Direction {

    NORTH, WEST, SOUTH, EAST;

    public Direction getOpposite() {
        return switch (this) {
            case NORTH -> SOUTH;
            case WEST -> EAST;
            case SOUTH -> NORTH;
            case EAST -> WEST;
        };
    }

}
