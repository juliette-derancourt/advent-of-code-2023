package aoc.common;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.function.Function;
import java.util.function.Predicate;

public record Grid<T>(List<List<T>> elements, int height, int width) {

    public static <T> Grid<T> parse(List<String> rows, Function<String, T> parser) {
        if (rows.isEmpty()) {
            return new Grid<>(List.of(), 0, 0);
        }
        List<List<T>> parsedElements = new ArrayList<>();
        for (String row : rows) {
            List<T> parsedRow = new ArrayList<>();
            for (char element : row.toCharArray()) {
                parsedRow.add(parser.apply(String.valueOf(element)));
            }
            parsedElements.add(parsedRow);
        }
        return new Grid<>(parsedElements, parsedElements.size(), parsedElements.get(0).size());
    }

    public List<List<T>> rows() {
        return elements;
    }

    public List<List<T>> columns() {
        return rotate().rows();
    }

    public Grid<T> rotate() {
        List<List<T>> columns = new ArrayList<>();
        for (int j = 0; j < elements.get(0).size(); j++) {
            List<T> column = new ArrayList<>();
            for (List<T> row : elements) {
                column.add(row.get(j));
            }
            columns.add(column);
        }
        return new Grid<>(columns, width, height);
    }

    public Coordinates findLocation(Predicate<T> predicate) {
        return find(predicate).stream()
                .map(Located::coordinates)
                .findFirst()
                .orElseThrow();
    }

    public List<Located<T>> find(Predicate<T> predicate) {
        return withCoordinates().stream()
                .filter(located -> predicate.test(located.element))
                .toList();
    }

    public List<Located<T>> withCoordinates() {
        List<Located<T>> elementsWithCoordinates = new ArrayList<>();
        for (int i = 0; i < height; i++) {
            for (int j = 0; j < width; j++) {
                elementsWithCoordinates.add(new Located<T>(elements.get(i).get(j), new Coordinates(i, j)));
            }
        }
        return elementsWithCoordinates;
    }

    public Optional<T> at(Coordinates coordinates) {
        if (!coordinates.isWithinBounds(height, width)) {
            return Optional.empty();
        }
        return Optional.ofNullable(elements.get(coordinates.x()).get(coordinates.y()));
    }

    public void swap(Coordinates c1, Coordinates c2) {
        T e1 = elements.get(c1.x()).get(c1.y());
        T e2 = elements.get(c2.x()).get(c2.y());
        elements.get(c1.x()).set(c1.y(), e2);
        elements.get(c2.x()).set(c2.y(), e1);
    }

    public String print() {
        return print(T::toString);
    }

    public String print(Function<T, String> printer) {
        StringBuilder stringBuilder = new StringBuilder();
        for (List<T> row : rows()) {
            for (T element : row) {
                stringBuilder.append(printer.apply(element));
            }
            stringBuilder.append("\n");
        }
        return stringBuilder.toString();
    }

    public record Located<T>(T element, Coordinates coordinates) {

    }

}
