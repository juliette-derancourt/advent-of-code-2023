package aoc.puzzles;

import aoc.PuzzleResolver;
import aoc.common.Coordinates;
import aoc.common.PuzzleInput;
import aoc.common.StringInput;
import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.stream.Collectors;

import static aoc.puzzles.Puzzle3.ElementType.GEAR_SYMBOL;
import static aoc.puzzles.Puzzle3.ElementType.NUMBER;
import static java.util.stream.IntStream.range;

public class Puzzle3 implements PuzzleResolver<Integer> {

    @Override
    public Integer part1(PuzzleInput input) {
        return Engine.parse(input)
                .getPartNumbers()
                .stream()
                .mapToInt(EngineElement::intValue)
                .sum();
    }

    @Override
    public Integer part2(PuzzleInput input) {
        return Engine.parse(input)
                .getGears()
                .stream()
                .mapToInt(Gear::getRatio)
                .sum();
    }

    record Engine(PuzzleInput input, List<EngineElement> elements) {

        static Engine parse(PuzzleInput input) {
            List<EngineElement> elements = new ArrayList<>();
            List<StringInput> lines = input.lines();
            for (int i = 0; i < lines.size(); i++) {
                Matcher matcher = lines.get(i).getMatcher("(\\d+|[^\\.])");
                while (matcher.find()) {
                    EngineElement element = EngineElement.build(matcher.group(), new Coordinates(i, matcher.start()));
                    elements.add(element);
                }
            }
            return new Engine(input, elements);
        }

        public List<EngineElement> getPartNumbers() {
            return elements.stream()
                    .filter(element -> element.type.equals(NUMBER))
                    .filter(element -> element.getAllNeighboursIn(input).stream()
                            .anyMatch(this::symbolExistsAt))
                    .toList();
        }

        boolean symbolExistsAt(Coordinates coordinates) {
            return getElement(coordinates).map(EngineElement::isSymbol).orElse(false);
        }

        Optional<EngineElement> getElement(Coordinates coordinates) {
            return elements.stream()
                    .filter(e -> e.coordinates.equals(coordinates))
                    .findFirst();
        }

        public List<Gear> getGears() {
            Map<EngineElement, Set<EngineElement>> partNumbersByGearSymbol = new HashMap<>();
            getPartNumbers().forEach(partNumber ->
                    getSurroundingGearSymbols(partNumber).forEach(gearSymbol ->
                            partNumbersByGearSymbol.computeIfAbsent(gearSymbol, key -> new HashSet<>()).add(partNumber)
                    )
            );
            return partNumbersByGearSymbol.values().stream()
                    .filter(numbers -> numbers.size() == 2)
                    .map(numbers -> numbers.stream().toList())
                    .map(numbers -> new Gear(numbers.get(0), numbers.get(1)))
                    .toList();
        }

        private Set<EngineElement> getSurroundingGearSymbols(EngineElement partNumber) {
            return partNumber.getAllNeighboursIn(input).stream()
                    .map(this::getElement)
                    .filter(Optional::isPresent)
                    .map(Optional::get)
                    .filter(element -> element.type == GEAR_SYMBOL)
                    .collect(Collectors.toSet());
        }

    }

    record EngineElement(ElementType type, String value, Coordinates coordinates) {

        static EngineElement build(String element, Coordinates coordinates) {
            ElementType type = ElementType.of(element);
            return new EngineElement(type, element, coordinates);
        }

        Set<Coordinates> getAllNeighboursIn(PuzzleInput input) {
            Set<Coordinates> elementCoordinates = range(0, value.length()).boxed()
                    .map(coordinates::moveY).collect(Collectors.toSet());
            return elementCoordinates.stream()
                    .flatMap(coordinate -> coordinate.getNeighboursWithinBounds(input.getHeight(), input.getWidth()).stream())
                    .filter(coordinate -> !elementCoordinates.contains(coordinate))
                    .collect(Collectors.toSet());
        }

        int intValue() {
            if (isSymbol()) {
                throw new RuntimeException("Not a number");
            }
            return Integer.parseInt(value);
        }

        boolean isSymbol() {
            return type != NUMBER;
        }

    }

    enum ElementType {

        NUMBER,
        GEAR_SYMBOL,
        SYMBOL;

        static ElementType of(String s) {
            if (StringUtils.isNumeric(s)) {
                return NUMBER;
            }
            return s.equals("*") ? GEAR_SYMBOL : SYMBOL;
        }

    }

    record Gear(EngineElement e1, EngineElement e2) {

        int getRatio() {
            return e1.intValue() * e2.intValue();
        }

    }

}
