package aoc.puzzles;

import aoc.PuzzleResolver;
import aoc.common.PuzzleInput;
import aoc.common.RegexMatch;
import aoc.common.StringInput;

import java.util.Arrays;
import java.util.List;

import static aoc.puzzles.Puzzle2.Color.BLUE;
import static aoc.puzzles.Puzzle2.Color.GREEN;
import static aoc.puzzles.Puzzle2.Color.RED;

public class Puzzle2 implements PuzzleResolver<Integer> {

    @Override
    public Integer part1(PuzzleInput input) {
        BagContent bagContent = new BagContent(List.of(
                new CubeSet(12, RED),
                new CubeSet(13, GREEN),
                new CubeSet(14, BLUE)
        ));
        return input.streamLines().map(Game::parse)
                .filter(game -> game.isPossibleWith(bagContent))
                .mapToInt(Game::id)
                .sum();
    }

    @Override
    public Integer part2(PuzzleInput input) {
        return input.streamLines().map(Game::parse)
                .map(Game::getMinimalBagContent)
                .mapToInt(BagContent::getPower)
                .sum();
    }

    record BagContent(List<CubeSet> cubes) {

        int getQuantityOf(Color color) {
            return cubes.stream()
                    .filter(c -> c.color == color)
                    .findFirst()
                    .map(CubeSet::quantity).orElse(0);
        }

        int getPower() {
            return cubes.stream()
                    .mapToInt(CubeSet::quantity)
                    .reduce(1, (a, b) -> a * b);
        }

    }

    record Game(int id, List<GameSet> gameSets) {

        static Game parse(StringInput line) {
            RegexMatch match = line.matchRegexOnce("Game (\\d+): (.*)");
            int id = match.getInt(1);
            List<GameSet> sets = match.get(2)
                    .split(";").stream()
                    .map(GameSet::parse).toList();
            return new Game(id, sets);
        }

        boolean isPossibleWith(BagContent bagContent) {
            return gameSets.stream().allMatch(set -> set.isPossibleWith(bagContent));
        }

        BagContent getMinimalBagContent() {
            List<CubeSet> content = Arrays.stream(Color.values())
                    .map(this::getMinimalSetForColor).toList();
            return new BagContent(content);
        }

        CubeSet getMinimalSetForColor(Color color) {
            int quantity = gameSets.stream()
                    .mapToInt(gameSet -> gameSet.getQuantityOf(color))
                    .max().orElse(0);
            return new CubeSet(quantity, color);
        }

    }

    record GameSet(List<CubeSet> revealedCubes) {

        static GameSet parse(StringInput string) {
            List<CubeSet> revealedCubes = string.matchRegex("(\\d+) ([a-z]+),?")
                    .stream()
                    .map(match -> CubeSet.of(match.getInt(1), match.getString(2)))
                    .toList();
            return new GameSet(revealedCubes);
        }

        boolean isPossibleWith(BagContent bagContent) {
            return revealedCubes.stream().allMatch(cubes -> cubes.isPossibleWith(bagContent));
        }

        int getQuantityOf(Color color) {
            return revealedCubes.stream()
                    .filter(cubeSet -> color == cubeSet.color)
                    .findFirst()
                    .map(CubeSet::quantity).orElse(0);
        }

    }

    record CubeSet(int quantity, Color color) {

        static CubeSet of(int quantity, String color) {
            return new CubeSet(quantity, Color.parse(color));
        }

        boolean isPossibleWith(BagContent bagContent) {
            return bagContent.getQuantityOf(color) >= quantity;
        }

    }

    enum Color {

        RED,
        BLUE,
        GREEN;

        static Color parse(String string) {
            return valueOf(string.toUpperCase());
        }

    }

}
