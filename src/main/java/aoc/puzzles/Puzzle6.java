package aoc.puzzles;

import aoc.PuzzleResolver;
import aoc.common.PuzzleInput;
import aoc.common.StringInput;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import static aoc.puzzles.Puzzle6.Race.parse;
import static aoc.puzzles.Puzzle6.Race.parseRaces;

public class Puzzle6 implements PuzzleResolver<Long> {

    @Override
    public Long part1(PuzzleInput input) {
        List<StringInput> lines = input.lines();
        List<Race> races = parseRaces(lines.get(0), lines.get(1));
        return races.stream()
                .map(Race::waysToWin)
                .reduce(1L, (a, b) -> a * b);
    }

    @Override
    public Long part2(PuzzleInput input) {
        List<StringInput> lines = input.lines();
        Race race = parse(lines.get(0), lines.get(1));
        return race.waysToWin();
    }

    record Race(long time, long recordDistance) {

        static Race parse(StringInput timeLine, StringInput recordDistanceLine) {
            String time = parseNumbersFromLine(timeLine).stream().map(String::valueOf).collect(Collectors.joining());
            String recordDistance = parseNumbersFromLine(recordDistanceLine).stream().map(String::valueOf).collect(Collectors.joining());
            return new Race(Long.parseLong(time), Long.parseLong(recordDistance));
        }

        static List<Race> parseRaces(StringInput timeLine, StringInput recordDistanceLine) {
            List<Integer> times = parseNumbersFromLine(timeLine);
            List<Integer> distances = parseNumbersFromLine(recordDistanceLine);
            List<Race> races = new ArrayList<>();
            for (int i = 0; i < times.size(); i++) {
                races.add(new Race(times.get(i), distances.get(i)));
            }
            return races;
        }

        private static List<Integer> parseNumbersFromLine(StringInput line) {
            return line.matchRegex("(\\d+)").stream().map(match -> match.getInt(1)).toList();
        }

        long waysToWin() {
            long numberOfWays = 0;
            for (int timeHoldingButton = 1; timeHoldingButton < time - 1; timeHoldingButton++) {
                long remainingTime = time - timeHoldingButton;
                int speed = timeHoldingButton;
                long distance = remainingTime * speed;
                if (distance > recordDistance) {
                    numberOfWays++;
                }
            }
            return numberOfWays;
        }

    }

}
