package aoc.puzzles;

import aoc.PuzzleResolver;
import aoc.common.Coordinates;
import aoc.common.PuzzleInput;
import aoc.common.StringInput;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.regex.Matcher;

public class Puzzle11 implements PuzzleResolver<Long> {

    @Override
    public Long part1(PuzzleInput input) {
        return Universe.parse(input, 2L).getSumOfShortestPaths();
    }

    @Override
    public Long part2(PuzzleInput input) {
        return Universe.parse(input, 1000000L).getSumOfShortestPaths();
    }

    record Universe(List<Coordinates> galaxies, long expansionRate,
                    List<Integer> expandedRows, List<Integer> expandedColumns) {

        static Universe parse(PuzzleInput input, long expansionRate) {
            List<Coordinates> galaxies = new ArrayList<>();
            List<StringInput> lines = input.lines();
            for (int i = 0; i < lines.size(); i++) {
                Matcher matcher = lines.get(i).getMatcher("#");
                while (matcher.find()) {
                    Coordinates coordinates = new Coordinates(i, matcher.start());
                    galaxies.add(coordinates);
                }
            }
            List<Integer> expandedRows = new ArrayList<>();
            for (int i = 0; i < lines.size(); i++) {
                if (!lines.get(i).contains("#")) {
                    expandedRows.add(i);
                }
            }
            List<Integer> expandedColumns = new ArrayList<>();
            AtomicInteger j = new AtomicInteger(0);
            while (j.incrementAndGet() < input.getWidth()) {
                List<String> column = lines.stream().map(line -> line.stringAt(j.get())).toList();
                if (!column.contains("#")) {
                    expandedColumns.add(j.get());
                }
            }
            return new Universe(galaxies, expansionRate, expandedRows, expandedColumns);
        }

        long getSumOfShortestPaths() {
            long sum = 0;
            for (int n = 0; n < galaxies.size(); n++) {
                for (int m = n + 1; m < galaxies.size(); m++) {
                    sum += findShortestPathBetween(galaxies.get(n), galaxies.get(m));
                }
            }
            return sum;
        }

        long findShortestPathBetween(Coordinates g1, Coordinates g2) {
            List<Integer> crossedRows = expandedRows.stream()
                    .filter(i -> (i < g1.x() && i > g2.x()) || (i > g1.x() && i < g2.x()))
                    .toList();
            long verticalExpansion = crossedRows.size() * (expansionRate - 1);
            List<Integer> crossedColumns = expandedColumns.stream()
                    .filter(i -> (i < g1.y() && i > g2.y()) || (i > g1.y() && i < g2.y()))
                    .toList();
            long horizontalExpansion = crossedColumns.size() * (expansionRate - 1);
            return Math.abs(g1.x() - g2.x()) + verticalExpansion +
                    Math.abs(g1.y() - g2.y()) + horizontalExpansion;
        }

    }

}
