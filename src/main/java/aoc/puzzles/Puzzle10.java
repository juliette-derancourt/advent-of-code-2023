package aoc.puzzles;

import aoc.PuzzleResolver;
import aoc.common.Coordinates;
import aoc.common.Direction;
import aoc.common.Grid;
import aoc.common.PuzzleInput;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Stream;

import static aoc.common.Direction.EAST;
import static aoc.common.Direction.NORTH;
import static aoc.common.Direction.SOUTH;
import static aoc.common.Direction.WEST;

public class Puzzle10 implements PuzzleResolver<Integer> {

    @Override
    public Integer part1(PuzzleInput input) {
        Field grid = Field.parse(input);
        return grid.findLoop().getFarthestPoint();
    }

    @Override
    public Integer part2(PuzzleInput input) {
        Field field = Field.parse(input);
        return field.countEnclosedByLoop();
    }

    record Field(Grid<Pipe> grid, Coordinates start) {

        static Field parse(PuzzleInput input) {
            Grid<Pipe> grid = input.toGrid(Pipe::of);
            return new Field(grid, grid.findLocation(pipe -> pipe.value.equals("S")));
        }

        Loop findLoop() {
            Movement currentMovement = getFirstMovement();
            Loop loop = Loop.startingWith(start);
            while (!currentMovement.leadTo(start)) {
                Coordinates currentCoordinates = currentMovement.getDestination();
                Direction nextDirection = grid.at(currentCoordinates).orElseThrow().getNextDirection(currentMovement.direction());
                currentMovement = new Movement(currentCoordinates, nextDirection);
                loop.add(currentMovement.origin);
            }
            return loop;
        }

        public Movement getFirstMovement() {
            return Stream.of(
                            new Movement(start, NORTH),
                            new Movement(start, WEST),
                            new Movement(start, SOUTH),
                            new Movement(start, EAST)
                    )
                    .filter(m -> {
                        Optional<Pipe> pipe = grid.at(m.getDestination());
                        return pipe.isPresent() && pipe.get().isReachableBy(m);
                    })
                    .findFirst()
                    .orElseThrow();
        }

        public int countEnclosedByLoop() {
            Loop loop = findLoop();
            int count = 0;
            boolean inside = false;
            String lastCurvedPipe = "";
            for (Grid.Located<Pipe> location : grid.withCoordinates()) {
                if (loop.contains(location.coordinates())) {
                    switch (location.element().value) {
                        case "|" -> inside = !inside;
                        case "L" -> lastCurvedPipe = "L";
                        case "F" -> lastCurvedPipe = "F";
                        case "J", "S" -> {
                            if (lastCurvedPipe.equals("F")) {
                                inside = !inside;
                            }
                            lastCurvedPipe = "";
                        }
                        case "7" -> {
                            if (lastCurvedPipe.equals("L")) {
                                inside = !inside;
                            }
                            lastCurvedPipe = "";
                        }
                    }
                } else if (inside) {
                    count++;
                }
            }
            return count;
        }

    }

    record Loop(List<Coordinates> coordinates) {

        static Loop startingWith(Coordinates start) {
            List<Coordinates> coordinates = new ArrayList<>();
            coordinates.add(start);
            return new Loop(coordinates);
        }

        void add(Coordinates coordinates) {
            this.coordinates.add(coordinates);
        }

        public int getFarthestPoint() {
            return coordinates.size() / 2;
        }

        public boolean contains(Coordinates coordinates) {
            return this.coordinates.contains(coordinates);
        }

    }

    record Pipe(String value, Set<Direction> connections) {

        static Pipe of(String value) {
            Set<Direction> connections = switch (value) {
                case "|" -> Set.of(NORTH, SOUTH);
                case "-" -> Set.of(WEST, EAST);
                case "L" -> Set.of(NORTH, EAST);
                case "J" -> Set.of(NORTH, WEST);
                case "7" -> Set.of(WEST, SOUTH);
                case "F" -> Set.of(EAST, SOUTH);
                default -> Set.of();
            };
            return new Pipe(value, connections);
        }

        boolean isReachableBy(Movement movement) {
            return connections.contains(movement.direction.getOpposite());
        }

        Direction getNextDirection(Direction direction) {
            return connections.stream()
                    .filter(c -> c != direction.getOpposite()).findFirst().orElseThrow();
        }

    }

    record Movement(Coordinates origin, Direction direction) {

        Coordinates getDestination() {
            return origin.move(direction);
        }

        boolean leadTo(Coordinates coordinates) {
            return getDestination().equals(coordinates);
        }

    }

}
