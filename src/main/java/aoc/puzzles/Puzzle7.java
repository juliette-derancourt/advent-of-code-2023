package aoc.puzzles;

import aoc.PuzzleResolver;
import aoc.common.PuzzleInput;
import aoc.common.StringInput;

import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static aoc.puzzles.Puzzle7.CardGroups.groupByType;
import static aoc.puzzles.Puzzle7.HandType.FIVE_OF_A_KIND;
import static aoc.puzzles.Puzzle7.HandType.FOUR_OF_A_KIND;
import static aoc.puzzles.Puzzle7.HandType.FULL_HOUSE;
import static aoc.puzzles.Puzzle7.HandType.HIGH_CARD;
import static aoc.puzzles.Puzzle7.HandType.ONE_PAIR;
import static aoc.puzzles.Puzzle7.HandType.THREE_OF_A_KIND;
import static aoc.puzzles.Puzzle7.HandType.TWO_PAIR;
import static aoc.puzzles.Puzzle7.ParsingType.CONSIDER_JOKERS;
import static aoc.puzzles.Puzzle7.ParsingType.DONT_CONSIDER_JOKERS;
import static java.util.Comparator.comparing;
import static java.util.Comparator.comparingLong;
import static java.util.stream.Collectors.collectingAndThen;
import static java.util.stream.Collectors.counting;

public class Puzzle7 implements PuzzleResolver<Integer> {

    @Override
    public Integer part1(PuzzleInput input) {
        return getTotalWinnings(input, DONT_CONSIDER_JOKERS);
    }

    @Override
    public Integer part2(PuzzleInput input) {
        return getTotalWinnings(input, CONSIDER_JOKERS);
    }

    private static int getTotalWinnings(PuzzleInput input, ParsingType parsingType) {
        List<PlayedHand> rankedHands = input.streamLines()
                .map(line -> PlayedHand.parse(line, parsingType))
                .sorted(comparing(PlayedHand::hand))
                .toList();

        return IntStream.range(0, rankedHands.size())
                .map(i -> rankedHands.get(i).getWinnings(i + 1))
                .sum();
    }

    record PlayedHand(Hand hand, int bid) {

        static PlayedHand parse(StringInput line, ParsingType parsingType) {
            List<StringInput> split = line.split(" ");
            Hand hand = Hand.parse(split.get(0), parsingType);
            int bid = split.get(1).toInt();
            return new PlayedHand(hand, bid);
        }

        int getWinnings(int rank) {
            return rank * bid;
        }

    }

    record Hand(List<Card> cards) implements Comparable<Hand> {

        static Hand parse(StringInput input, ParsingType parsingType) {
            String cardsAsString = switch (parsingType) {
                case DONT_CONSIDER_JOKERS -> input.value();
                case CONSIDER_JOKERS -> input.value().replace("J", "j");
            };
            var cards = cardsAsString.chars().mapToObj(c -> new Card((char) c)).toList();
            return new Hand(cards);
        }

        @Override
        public int compareTo(Hand other) {
            return comparingLong(Hand::getSortingScore).compare(this, other);
        }

        private long getSortingScore() {
            String cardsScore = cards.stream().map(Card::getScore)
                    .map(cardScore -> String.format("%02d", cardScore))
                    .collect(Collectors.joining());
            return Long.parseLong(getHandType().score + cardsScore);
        }

        private HandType getHandType() {
            CardGroups cardGroups = groupByType(cards);
            return switch (cardGroups.sizeOfBiggestGroup()) {
                case 5 -> FIVE_OF_A_KIND;
                case 4 -> FOUR_OF_A_KIND;
                case 3 -> cardGroups.count() == 2 ? FULL_HOUSE : THREE_OF_A_KIND;
                case 2 -> cardGroups.count() == 3 ? TWO_PAIR : ONE_PAIR;
                default -> HIGH_CARD;
            };
        }

    }

    record CardGroups(Map<Card, Integer> frequency) {

        static CardGroups groupByType(List<Card> cards) {
            var frequency = cards.stream()
                    .collect(Collectors.groupingBy(Function.identity(), collectingAndThen(counting(), Long::intValue)));
            return new CardGroups(frequency);
        }

        int sizeOfBiggestGroup() {
            return sizeOfBiggestNonJokerGroup() + jokerCount();
        }

        private int sizeOfBiggestNonJokerGroup() {
            return frequency.entrySet()
                    .stream()
                    .filter(entry -> entry.getKey().isNotJoker())
                    .mapToInt(Map.Entry::getValue)
                    .max().orElse(0);
        }

        private int jokerCount() {
            return Optional.ofNullable(frequency.get(Card.JOKER)).orElse(0);
        }

        int count() {
            return (int) frequency.keySet().stream().filter(Card::isNotJoker).count();
        }

    }

    record HandType(int score) {

        static HandType FIVE_OF_A_KIND = new HandType(6);
        static HandType FOUR_OF_A_KIND = new HandType(5);
        static HandType FULL_HOUSE = new HandType(4);
        static HandType THREE_OF_A_KIND = new HandType(3);
        static HandType TWO_PAIR = new HandType(2);
        static HandType ONE_PAIR = new HandType(1);
        static HandType HIGH_CARD = new HandType(0);

    }

    record Card(char symbol) {

        static Card JOKER = new Card('j');

        int getScore() {
            List<Character> rankedCards = List.of('j', '2', '3', '4', '5', '6', '7', '8', '9', 'T', 'J', 'Q', 'K', 'A');
            return rankedCards.indexOf(symbol);
        }

        boolean isNotJoker() {
            return !equals(JOKER);
        }

    }

    enum ParsingType {

        DONT_CONSIDER_JOKERS,
        CONSIDER_JOKERS

    }

}
