package aoc.puzzles;

import aoc.PuzzleResolver;
import aoc.common.PuzzleInput;
import aoc.common.StringInput;

import java.util.List;

import static aoc.common.StringInput.input;

public class Puzzle12 implements PuzzleResolver<Integer> {

    @Override
    public Integer part1(PuzzleInput input) {
        return input.streamLines()
                .map(ConditionRecord::parse)
                .mapToInt(ConditionRecord::getPossibleArrangements)
                .sum();
    }

    @Override
    public Integer part2(PuzzleInput input) {
        return null;
    }

    record ConditionRecord(String springs, List<Integer> groupsOfDamagedSprings) {

        private static final char OPERATIONAL = '.';
        private static final char DAMAGED = '#';
        private static final char UNKNOWN = '?';

        static ConditionRecord parse(StringInput line) {
            List<StringInput> split = line.split(" ");
            String springs = split.get(0).value();
            List<Integer> groupsOfDamagedSprings = split.get(1).split(",").stream().map(StringInput::toInt).toList();
            return new ConditionRecord(springs, groupsOfDamagedSprings);
        }

        int getPossibleArrangements() {
            return countPossibleArrangements(springs, 0);
        }

        private int countPossibleArrangements(String springs, int count) {
            for (int i = 0; i < springs.length(); i++) {
                if (springs.charAt(i) == UNKNOWN) {
                    var possibility1 = new StringBuilder(springs);
                    possibility1.setCharAt(i, OPERATIONAL);
                    var possibility2 = new StringBuilder(springs);
                    possibility2.setCharAt(i, DAMAGED);
                    return countPossibleArrangements(possibility1.toString(), count) + countPossibleArrangements(possibility2.toString(), count);
                }
            }
            return count + (isCompatibleWithDamagedGroupsOf(springs) ? 1 : 0);
        }

        private boolean isCompatibleWithDamagedGroupsOf(String springs) {
            List<Integer> groups = input(springs).matchRegex("(#+)").stream()
                    .map(match -> match.getString(1))
                    .map(String::length).toList();
            if (groups.size() != groupsOfDamagedSprings.size()) {
                return false;
            }
            for (int i = 0; i < groups.size(); i++) {
                if (!groups.get(i).equals(groupsOfDamagedSprings.get(i))) {
                    return false;
                }
            }
            return true;
        }

    }

}
