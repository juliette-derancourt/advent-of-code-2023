package aoc.puzzles;

import aoc.PuzzleResolver;
import aoc.common.PuzzleInput;
import aoc.common.StringInput;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.LongStream;

public class Puzzle5 implements PuzzleResolver<Long> {

    @Override
    public Long part1(PuzzleInput input) {
        Almanac almanac = Almanac.parse(input);
        return almanac.getLowestLocation();
    }

    @Override
    public Long part2(PuzzleInput input) {
        Almanac almanac = Almanac.parseWithRanges(input);
        return almanac.getLowestLocation();
    }

    record Almanac(Seeds seeds, List<SourceToDestinationMap> maps) {

        static Almanac parse(PuzzleInput puzzleInput) {
            return new Almanac(Seeds.parseSeeds(puzzleInput), parseMaps(puzzleInput));
        }

        static Almanac parseWithRanges(PuzzleInput puzzleInput) {
            return new Almanac(Seeds.parseSeedRanges(puzzleInput), parseMaps(puzzleInput));
        }

        private static List<SourceToDestinationMap> parseMaps(PuzzleInput puzzleInput) {
            List<StringInput> split = puzzleInput.linesAsString().split(":\n");
            return split.subList(1, split.size())
                    .stream()
                    .map(SourceToDestinationMap::parse)
                    .toList();
        }

        long getLowestLocation() {
            return seeds.ranges.stream()
                    .mapToLong(this::getLowestLocationFor)
                    .min().orElse(Long.MAX_VALUE);
        }

        long getLowestLocationFor(SeedRange range) {
            long[] result = range.getSeeds();
            for (SourceToDestinationMap map : maps) {
                result = map.getDestination(result);
            }
            return Arrays.stream(result).min().orElse(Long.MAX_VALUE);
        }

    }

    record SourceToDestinationMap(MapRow[] rows) {

        static SourceToDestinationMap parse(StringInput input) {
            MapRow[] rows = input.matchRegex("(\\d+.*)")
                    .stream().map(match -> match.get(1))
                    .map(MapRow::parse)
                    .toArray(MapRow[]::new);
            return new SourceToDestinationMap(rows);
        }

        long[] getDestination(long[] source) {
            return Arrays.stream(source).parallel()
                    .map(this::getDestination).toArray();
        }

        long getDestination(long source) {
            for (MapRow line : rows) {
                if (source >= line.sourceRangeStart && source < line.sourceRangeStart + line.range) {
                    return line.destinationRangeStart + (source - line.sourceRangeStart);
                }
            }
            return source;
        }

    }

    record MapRow(long destinationRangeStart, long sourceRangeStart, long range) {

        static MapRow parse(StringInput line) {
            List<StringInput> numbers = line.split(" ");
            long destinationRangeStart = numbers.get(0).toLong();
            long sourceRangeStart = numbers.get(1).toLong();
            long range = numbers.get(2).toLong();
            return new MapRow(destinationRangeStart, sourceRangeStart, range);
        }

    }

    record Seeds(List<SeedRange> ranges) {

        static Seeds parseSeeds(PuzzleInput puzzleInput) {
            List<SeedRange> ranges = puzzleInput.lines().get(0)
                    .matchRegexOnce(": (.*)$").get(1).split(" ")
                    .stream()
                    .map(StringInput::toLong)
                    .map(seed -> new SeedRange(seed, 1))
                    .toList();
            return new Seeds(ranges);
        }

        static Seeds parseSeedRanges(PuzzleInput puzzleInput) {
            String[] seedNumbers = puzzleInput.lines().get(0)
                    .value().substring(7).split(" ");
            List<SeedRange> ranges = new ArrayList<>();
            for (int i = 0; i < seedNumbers.length; i += 2) {
                long start = Long.parseLong(seedNumbers[i]);
                long range = Long.parseLong(seedNumbers[i + 1]);
                ranges.add(new SeedRange(start, range));
            }
            return new Seeds(ranges);
        }

    }

    record SeedRange(long start, long range) {

        long[] getSeeds() {
            return LongStream.range(0, range)
                    .map(n -> start + n).toArray();
        }

    }

}
