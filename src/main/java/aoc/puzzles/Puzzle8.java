package aoc.puzzles;

import aoc.PuzzleResolver;
import aoc.common.MathUtils;
import aoc.common.PuzzleInput;
import aoc.common.StringInput;
import org.apache.commons.lang3.tuple.Pair;

import java.math.BigInteger;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Predicate;

public class Puzzle8 implements PuzzleResolver<BigInteger> {

    @Override
    public BigInteger part1(PuzzleInput input) {
        return Maps.parse(input).navigate(
                /* from */ node -> node.equals("AAA"),
                /* until */ node -> node.equals("ZZZ")
        );
    }

    @Override
    public BigInteger part2(PuzzleInput input) {
        return Maps.parse(input).navigate(
                /* from */ node -> node.endsWith("A"),
                /* until */ node -> node.endsWith("Z")
        );
    }

    record Maps(List<Direction> directions, Network network) {

        static Maps parse(PuzzleInput input) {
            List<StringInput> lines = input.lines();
            return new Maps(Direction.parseList(lines), Network.parse(input));
        }

        BigInteger navigate(Predicate<String> startingNodes, Predicate<String> endingNodes) {
            return network.findNode(startingNodes)
                    .stream()
                    .map(node -> navigateUntil(node, endingNodes))
                    .map(BigInteger::valueOf)
                    .reduce(BigInteger.ONE, MathUtils::lcm);
        }

        private int navigateUntil(String startingNode, Predicate<String> predicate) {
            String currentNode = startingNode;
            int steps = 0;
            while (predicate.negate().test(currentNode)) {
                Direction direction = directions.get(steps % directions.size());
                currentNode = network.getNextNode(currentNode, direction);
                steps++;
            }
            return steps;
        }

    }

    record Network(Map<String, Pair<String, String>> nodes) {

        static Network parse(PuzzleInput input) {
            Map<String, Pair<String, String>> nodes = new HashMap<>();
            input.linesAsString()
                    .matchRegex("(\\w{3}) = \\((\\w{3}), (\\w{3})\\)")
                    .forEach(match -> nodes.put(match.getString(1), Pair.of(match.getString(2), match.getString(3))));
            return new Network(nodes);
        }

        String getNextNode(String currentNode, Direction direction) {
            Pair<String, String> options = nodes.get(currentNode);
            return direction == Direction.LEFT ? options.getLeft() : options.getRight();
        }

        List<String> findNode(Predicate<String> predicate) {
            return nodes.keySet().stream().filter(predicate).toList();
        }

    }

    enum Direction {

        LEFT,
        RIGHT;

        static List<Direction> parseList(List<StringInput> lines) {
            return lines.get(0).split("")
                    .stream()
                    .map(letter -> letter.value().equals("L") ? LEFT : RIGHT)
                    .toList();
        }

    }

}
