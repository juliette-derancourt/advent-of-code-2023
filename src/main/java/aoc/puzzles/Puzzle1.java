package aoc.puzzles;

import aoc.PuzzleResolver;
import aoc.common.PuzzleInput;
import aoc.common.StringInput;

import java.util.List;
import java.util.Objects;
import java.util.Optional;

public class Puzzle1 implements PuzzleResolver<Integer> {

    @Override
    public Integer part1(PuzzleInput input) {
        return input.streamLines()
                .map(line -> new Calibration(line, false))
                .mapToInt(Calibration::getValue)
                .sum();
    }

    @Override
    public Integer part2(PuzzleInput input) {
        return input.streamLines()
                .map(line -> new Calibration(line, true))
                .mapToInt(Calibration::getValue)
                .sum();
    }

    static class Calibration {

        private final StringInput line;
        private final boolean replaceWords;

        public Calibration(StringInput line, boolean replaceWords) {
            this.line = line;
            this.replaceWords = replaceWords;
        }

        int getValue() {
            Optional<String> first = findFirstDigit();
            Optional<String> last = findLastDigit();
            if (first.isEmpty() || last.isEmpty()) {
                return 0;
            }
            return Integer.parseInt(first.get() + last.get());
        }

        Optional<String> findFirstDigit() {
            return line.indexes()
                    .map(i -> line.substringWithinBounds(i, i + 5))
                    .flatMap(substring -> findDigitsIn(substring).stream())
                    .filter(Objects::nonNull)
                    .findFirst();
        }

        Optional<String> findLastDigit() {
            return line.indexes()
                    .map(i -> line.substringWithinBounds(i - 5, i))
                    .map(this::findDigitsIn)
                    .filter(digits -> !digits.isEmpty())
                    .map(digits -> digits.get(digits.size() - 1))
                    .reduce((first, last) -> last);
        }

        private List<String> findDigitsIn(String string) {
            String stringToSearch = replaceWords ? replaceWordsByDigits(string) : string;
            return stringToSearch.chars()
                    .mapToObj(c -> (char) c)
                    .filter(Character::isDigit)
                    .map(String::valueOf)
                    .toList();
        }

        private String replaceWordsByDigits(String string) {
            return string
                    .replace("one", "1")
                    .replace("two", "2")
                    .replace("three", "3")
                    .replace("four", "4")
                    .replace("five", "5")
                    .replace("six", "6")
                    .replace("seven", "7")
                    .replace("eight", "8")
                    .replace("nine", "9");
        }

    }

}
