package aoc.puzzles;

import aoc.PuzzleResolver;
import aoc.common.PuzzleInput;
import aoc.common.RegexMatch;
import aoc.common.StringInput;

import java.util.ArrayDeque;
import java.util.Deque;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class Puzzle4 implements PuzzleResolver<Integer> {

    @Override
    public Integer part1(PuzzleInput input) {
        return input.streamLines()
                .map(Card::parse)
                .mapToInt(Card::getScore)
                .sum();
    }

    @Override
    public Integer part2(PuzzleInput input) {
        Map<Integer, Card> cardsCollection = input.streamLines()
                .map(Card::parse)
                .collect(Collectors.toMap(Card::number, Function.identity()));

        Deque<Card> cardsToScratch = new ArrayDeque<>(cardsCollection.values());
        int scratchedCards = 0;

        while (!cardsToScratch.isEmpty()) {
            Card card = cardsToScratch.removeFirst();
            scratchedCards++;
            List<Card> winCopies = card.getWinCopiesNumbers().stream().map(cardsCollection::get).toList();
            cardsToScratch.addAll(winCopies);
        }

        return scratchedCards;
    }

    record Card(int number, List<Integer> winningNumbers, List<Integer> scratchedNumbers) {

        static Card parse(StringInput input) {
            RegexMatch regexMatch = input.matchRegexOnce("(\\d+): ([^\\|]*)\\|(.*)");
            return new Card(regexMatch.getInt(1), parseNumbers(regexMatch.get(2)), parseNumbers(regexMatch.get(3)));
        }

        static List<Integer> parseNumbers(StringInput input) {
            List<RegexMatch> regexMatches = input.matchRegex("(\\d+)");
            return regexMatches.stream().map(match -> match.getInt(1)).toList();
        }

        int getScore() {
            return (int) Math.pow(2, getMatchingNumbersCount() - 1);
        }

        List<Integer> getWinCopiesNumbers() {
            return IntStream.rangeClosed(1, getMatchingNumbersCount())
                    .mapToObj(n -> n + number)
                    .toList();
        }

        private int getMatchingNumbersCount() {
            return (int) scratchedNumbers.stream()
                    .filter(winningNumbers::contains)
                    .count();
        }

    }

}
