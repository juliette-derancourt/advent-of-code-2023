package aoc.puzzles;

import aoc.PuzzleResolver;
import aoc.common.Coordinates;
import aoc.common.Direction;
import aoc.common.Grid;
import aoc.common.Grid.Located;
import aoc.common.PuzzleInput;

import java.util.ArrayList;
import java.util.List;

import static aoc.common.Direction.NORTH;

public class Puzzle14 implements PuzzleResolver<Integer> {

    @Override
    public Integer part1(PuzzleInput input) {
        Platform platform = new Platform(input.toGrid());
        platform.tilt(NORTH);
        return platform.getLoadOnNorthSupportBeams();
    }

    @Override
    public Integer part2(PuzzleInput input) {
        Platform platform = new Platform(input.toGrid());
        for (int i = 0; i < 1000; i++) {
            platform.spinCycle();
        }
        return platform.getLoadOnNorthSupportBeams();
    }

    record Platform(Grid<String> grid) {

        void spinCycle() {
            for (Direction value : Direction.values()) {
                tilt(value);
            }
        }

        void tilt(Direction direction) {
            for (Located<String> roundedRock : findRoundedRocksInOrder(direction)) {
                Coordinates coordinates = roundedRock.coordinates();
                while (coordinates.move(direction).isWithinBounds(grid) &&
                        grid.at(coordinates.move(direction)).map(s -> s.equals(".")).orElse(false)) {
                    coordinates = coordinates.move(direction);
                }
                grid.swap(roundedRock.coordinates(), coordinates);
            }
        }

        int getLoadOnNorthSupportBeams() {
            return findRoundedRocksInOrder(NORTH).stream()
                    .map(Located::coordinates)
                    .mapToInt(coordinates -> grid.height() - coordinates.x())
                    .sum();
        }

        private List<Located<String>> findRoundedRocksInOrder(Direction direction) {
            List<Located<String>> list = new ArrayList<>(grid.find(s -> s.equals("O")));
            list.sort((a, b) -> switch (direction) {
                case NORTH -> a.coordinates().x() - b.coordinates().x();
                case WEST -> a.coordinates().y() - b.coordinates().y();
                case SOUTH -> b.coordinates().x() - a.coordinates().x();
                case EAST -> b.coordinates().y() - a.coordinates().y();
            });
            return list;
        }

    }

}
