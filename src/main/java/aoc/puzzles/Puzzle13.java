package aoc.puzzles;

import aoc.PuzzleResolver;
import aoc.common.Grid;
import aoc.common.PuzzleInput;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.function.Function;

import static aoc.puzzles.Puzzle13.SummarizeMethod.*;
import static aoc.puzzles.Puzzle13.SummarizeMethod.WITH_SMUDGES;

public class Puzzle13 implements PuzzleResolver<Integer> {

    @Override
    public Integer part1(PuzzleInput input) {
        return Notes.parse(input, WITHOUT_SMUDGES).summarize();
    }

    @Override
    public Integer part2(PuzzleInput input) {
        return Notes.parse(input, WITH_SMUDGES).summarize();
    }

    record Notes(List<Grid<String>> patterns, SummarizeMethod method) {

        static Notes parse(PuzzleInput input, SummarizeMethod method) {
            List<Grid<String>> patterns = input.linesAsString()
                    .split("\n\n").stream()
                    .map(s -> s.value().lines().toList())
                    .map(s -> Grid.parse(s, Function.identity()))
                    .toList();
            return new Notes(patterns, method);
        }

        int summarize() {
            return patterns.stream()
                    .mapToInt(pattern -> findColumnsToTheLeftOfVerticalReflection(pattern)
                            + 100 * findRowsAboveHorizontalReflection(pattern))
                    .sum();
        }

        private int findColumnsToTheLeftOfVerticalReflection(Grid<String> pattern) {
            return Mirror.of(pattern.columns(), method)
                    .findLineOfReflectionIndex()
                    .map(i -> i + 1).orElse(0);
        }

        private int findRowsAboveHorizontalReflection(Grid<String> pattern) {
            return Mirror.of(pattern.rows(), method)
                    .findLineOfReflectionIndex()
                    .map(i -> i + 1).orElse(0);
        }

    }

    record Mirror(List<List<String>> lines, boolean hasSmudge) {

        static Mirror of(List<List<String>> lines, SummarizeMethod method) {
            return new Mirror(lines, method == WITH_SMUDGES);
        }

        private Optional<Integer> findLineOfReflectionIndex() {
            for (int i = 0; i < lines.size() - 1; i++) {
                if (isLineOfReflection(i)) {
                    return Optional.of(i);
                }
            }
            return Optional.empty();
        }

        private boolean isLineOfReflection(int i) {
            int n = i;
            int m = i + 1;
            boolean isLineOfReflection = true;
            boolean foundSmudge = !hasSmudge;
            while (n >= 0 && m < lines.size() && isLineOfReflection) {
                List<String> line = lines.get(n);
                List<String> reflectedLine = lines.get(m);
                if (!line.equals(reflectedLine)) {
                    if (areEqualWithSmudge(line, reflectedLine) && !foundSmudge) {
                        foundSmudge = true;
                    } else {
                        isLineOfReflection = false;
                    }
                }
                n--;
                m++;
            }
            return isLineOfReflection && foundSmudge;
        }

        private boolean areEqualWithSmudge(List<String> l1, List<String> l2) {
            for (int i = 0; i < l1.size(); i++) {
                ArrayList<String> l1copy = new ArrayList<>(l1);
                ArrayList<String> l2copy = new ArrayList<>(l2);
                l1copy.remove(i);
                l2copy.remove(i);
                if (l1copy.equals(l2copy)) {
                    return true;
                }
            }
            return false;
        }

    }

    enum SummarizeMethod {
        WITHOUT_SMUDGES,
        WITH_SMUDGES
    }

}
