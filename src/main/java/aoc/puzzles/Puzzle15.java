package aoc.puzzles;

import aoc.PuzzleResolver;
import aoc.common.PuzzleInput;
import aoc.common.RegexMatch;
import aoc.common.StringInput;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.IntStream;

public class Puzzle15 implements PuzzleResolver<Integer> {

    @Override
    public Integer part1(PuzzleInput input) {
        return input.lines().get(0).split(",").stream()
                .map(StringInput::value)
                .mapToInt(Puzzle15::hashAlgorithm)
                .sum();
    }

    @Override
    public Integer part2(PuzzleInput input) {
        Boxes boxes = Boxes.init();
        input.lines().get(0).split(",").forEach(boxes::accept);
        return boxes.getTotalFocusingPower();
    }

    static int hashAlgorithm(String string) {
        int currentValue = 0;
        for (int asciiCode : string.chars().toArray()) {
            currentValue += asciiCode;
            currentValue *= 17;
            currentValue %= 256;
        }
        return currentValue;
    }

    record Boxes(List<Box> boxes) {

        static Boxes init() {
            return new Boxes(IntStream.rangeClosed(0, 255)
                    .mapToObj(i -> new Box(i, new ArrayList<>())).toList());
        }

        void accept(StringInput command) {
            RegexMatch regexMatch = command.matchRegexOnce("(\\w+)([=-])(\\d*)");
            String label = regexMatch.getString(1);
            int boxId = hashAlgorithm(label);
            Box box = boxes.get(boxId);
            String symbol = regexMatch.getString(2);
            if (symbol.equals("-")) {
                box.removeLensWith(label);
            } else {
                int length = regexMatch.getInt(3);
                box.addOrReplace(new Lens(label, length));
            }
        }

        int getTotalFocusingPower() {
            return boxes.stream().mapToInt(Box::getFocusingPower).sum();
        }

    }

    record Box(int id, List<Lens> content) {

        void removeLensWith(String label) {
            content.stream().filter(lens -> lens.label.equals(label)).findFirst().ifPresent(content::remove);
        }

        public void addOrReplace(Lens lensToAdd) {
            for (int i = 0; i < content.size(); i++) {
                if (content.get(i).label.equals(lensToAdd.label)) {
                    content.set(i, lensToAdd);
                    return;
                }
            }
            content.add(lensToAdd);
        }

        int getFocusingPower() {
            int power = 0;
            for (int i = 0; i < content.size(); i++) {
                power += (id + 1) * (i + 1) * content.get(i).focalLength;
            }
            return power;
        }

    }

    record Lens(String label, int focalLength) {

    }

}
