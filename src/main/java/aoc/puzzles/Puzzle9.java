package aoc.puzzles;

import aoc.PuzzleResolver;
import aoc.common.PuzzleInput;
import aoc.common.StringInput;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Predicate;

public class Puzzle9 implements PuzzleResolver<Integer> {

    @Override
    public Integer part1(PuzzleInput input) {
        return input.streamLines().map(History::parse)
                .mapToInt(History::extrapolateNextValue)
                .sum();
    }

    @Override
    public Integer part2(PuzzleInput input) {
        return input.streamLines().map(History::parse)
                .mapToInt(History::extrapolatePreviousValue)
                .sum();
    }

    record History(List<Sequence> sequences) {

        public static History parse(StringInput input) {
            return new History(computeSequences(input.extractNumbers()));
        }

        private static List<Sequence> computeSequences(List<Integer> startValues) {
            List<Sequence> sequences = new ArrayList<>();
            Sequence current = new Sequence(startValues);
            while (current.isNotAllZeros()) {
                sequences.add(current);
                current = current.getDiffSequence();
            }
            return sequences;
        }

        public int extrapolateNextValue() {
            return sequences.stream().mapToInt(Sequence::getLastElement).sum();
        }

        public int extrapolatePreviousValue() {
            AtomicInteger index = new AtomicInteger(0);
            Predicate<AtomicInteger> isPair = i -> i.getAndIncrement() % 2 == 0;

            return sequences.stream()
                    .map(Sequence::getFirstElement)
                    .reduce(0, (a, b) -> isPair.test(index) ? a + b : a - b);
        }

    }

    record Sequence(List<Integer> values) {

        Sequence getDiffSequence() {
            List<Integer> diff = new ArrayList<>();
            for (int i = 1; i < values.size(); i++) {
                diff.add(values.get(i) - values.get(i - 1));
            }
            return new Sequence(diff);
        }

        boolean isNotAllZeros() {
            return values.stream().anyMatch(v -> v != 0);
        }

        int getFirstElement() {
            return values.get(0);
        }

        int getLastElement() {
            return values.get(values.size() - 1);
        }

    }

}
